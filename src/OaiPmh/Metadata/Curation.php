<?php declare(strict_types=1);
/**
 * @author John Flatness, Yu-Hsun Lin
 * @copyright Copyright 2009 John Flatness, Yu-Hsun Lin
 * @copyright BibLibre, 2016
 * @copyright Daniel Berthereau, 2014-2018
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
namespace OaiPmhRepository\OaiPmh\Metadata;

use DOMElement;
use Omeka\Api\Representation\ItemRepresentation;

/**
 * Class implementing metadata output for the curation metadata format.
 * curation is output of the 55 Dublin Core terms.
 *
 * This format is not standardized, but used by some repositories.
 * Note: the namespace and the schema don’t exist. It is designed as an extended
 * version of oai_dc.
 *
 * @link http://www.bl.uk/schemas/
 * @link http://dublincore.org/documents/dc-xml-guidelines/
 * @link http://dublincore.org/schemas/xmls/qdc/dcterms.xsd
 */
class Curation extends AbstractMetadata
{
    /** OAI-PMH metadata prefix */
    const METADATA_PREFIX = 'curation';

    /** XML namespace for output format */
    const METADATA_NAMESPACE = 'http://www.openarchives.org/OAI/2.0/curation/';

    /** XML schema for output format */
    const METADATA_SCHEMA = 'http://www.openarchives.org/OAI/2.0/curation.xsd';

    /** XML namespace for Dublin Core */
    const curation_NAMESPACE_URI = 'http://purl.org/curation/terms/';

    const prefix = "https://curation.library.t.u-tokyo.ac.jp";

    /**
     * Appends Dublin Core terms metadata.
     *
     * {@inheritDoc}
     */
    public function appendMetadata(DOMElement $metadataElement, ItemRepresentation $item): void
    {
        $document = $metadataElement->ownerDocument;

        $oai = $document->createElementNS(self::METADATA_NAMESPACE, 'curation:curation');
        $metadataElement->appendChild($oai);

        /* Must manually specify XML schema uri per spec, but DOM won't include
         * a redundant xmlns:xsi attribute, so we just set the attribute
         */
        $oai->setAttribute('xmlns:dc', self::curation_NAMESPACE_URI);
        $oai->setAttribute('xmlns:xsi', parent::XML_SCHEMA_NAMESPACE_URI);
        $oai->setAttribute('xsi:schemaLocation', self::METADATA_NAMESPACE . ' ' .
            self::METADATA_SCHEMA);

        /* Each of the 15 unqualified Dublin Core elements, in the order
         * specified by the curation XML schema
         */
        $localNames = [
            'title',
            'creator',
            'subject',
            'description',
            'publisher',
            'contributor',
            'date',
            'type',
            'format',
            'identifier',
            'source',
            'language',
            'relation',
            'coverage',
            'rights',
        ];

        /* Must create elements using createElement to make DOM allow a
         * top-level xmlns declaration instead of wasteful and non-
         * compliant per-node declarations.
         */
        /*
        foreach ($localNames as $localName) {
            $term = 'dcterms:' . $localName;
            $values = $item->value($term, ['all' => true, 'default' => []]);
            $values = $this->filterValues($item, $term, $values);
            foreach ($values as $value) {
                if($value != ""){
                    $this->appendNewElement($oai, 'curation:' . $localName, (string) $value);
                }
            }
        }
        */

        if ( $item->value( "dcterms:title" ) ) {
            $this->appendNewElement($oai, 'curation:' . "title", (string)$item->value("dcterms:title")->value());
        }

        if ( $item->value( "dcterms:creator" ) ) {
            $this->appendNewElement($oai, 'curation:' . "creator", (string)$item->value("dcterms:creator")->value());
        }

        if ( $item->value( "dcterms:format" ) ) {
            $this->appendNewElement($oai, 'curation:' . "format", (string)$item->value("dcterms:format")->value());
        }

        if ( $item->value( "dcterms:extent" ) ) {
            $this->appendNewElement($oai, 'curation:' . "format", (string)$item->value("dcterms:extent")->value());
        }

        if ( $item->value( "dcterms:publisher" ) ) {
            $this->appendNewElement($oai, 'curation:' . "publisher", (string)$item->value("dcterms:publisher")->value());
        }

        if ( $item->value( "dcterms:date" ) ) {
            $date = (string)$item->value("dcterms:date")->value();
            $this->appendNewElement($oai, 'curation:' . "issued", $date);
            $tmp = explode ("-", $date);
            if (ctype_digit($tmp[0])) {
                $year = intval($tmp[0]);
                if ($year >= 100){
                    $this->appendNewElement($oai, 'curation:' . "year", $tmp[0]);
                }
            }
        }

        if ( $item->value( "dcterms:identifier" ) ) {
            $this->appendNewElement($oai, 'curation:' . "relation", self::prefix."/s/db/record/".(string)$item->value("dcterms:identifier")->value());
        }

        if ( $item->value( "dcterms:rights" ) ) {
            $this->appendNewElement($oai, 'curation:' . "rights", (string)$item->value("dcterms:rights")->uri());
        }

        $mediaList = $item->media();
        if (!empty($mediaList)) {
            $this->appendNewElement($oai, 'curation:' . "thumbnail", str_replace("https", "http", $mediaList[0]->thumbnailUrl("medium")));

            $this->appendNewElement($oai, 'curation:' . "manifestUri", self::prefix."/iiif/".(string)$item->id()."/manifest");
        } else {
            if ( $item->value( "foaf:thumbnail" ) ) {
                $this->appendNewElement($oai, 'curation:' . "thumbnail", str_replace("https", "http", (string)$item->value("foaf:thumbnail")->uri()));
            }
        }

        $this->appendNewElement($oai, 'curation:' . "seeAlso", self::prefix."/api/items/".(string)$item->id());

        $terms = [
            "dcterms:title", "dcterms:creator", "dcterms:extent", "dcterms:format",
            "dcterms:identifier", "archiveshub:dateCreatedAccumulatedString",
            "dcterms:date", "sc:attributionLabel", "dcterms:publisher"
        ];

        foreach ($item->values() as $term => $propertyData){
            //var_dump($term);

            if (in_array($term, $terms)){
                continue;
            }

            $p = "";

            if ($propertyData['alternate_label']){
                $p = $propertyData['alternate_label'];
            } else {
                $p = $propertyData['property']->label();
            }

            foreach($propertyData['values'] as $value){
                $v = $value->value();
                if($v != ""){
                    $this->appendNewElement($oai, 'curation:' . "description", $p.": ".$v);
                }

            }

        }

        /*
        $appendIdentifier = $this->singleIdentifier($item);
        if ($appendIdentifier) {
            $this->appendNewElement($oai, 'curation:identifier', $appendIdentifier);
        }

        // Also append an identifier for each file
        if ($this->settings->get('oaipmhrepository_expose_media', false)) {
            foreach ($item->media() as $media) {
                $this->appendNewElement($oai, 'curation:identifier', $media->originalUrl());
            }
        }
        */
    }

    public function getMetadataPrefix()
    {
        return self::METADATA_PREFIX;
    }

    public function getMetadataSchema()
    {
        return self::METADATA_SCHEMA;
    }

    public function getMetadataNamespace()
    {
        return self::METADATA_NAMESPACE;
    }
}