<?php declare(strict_types=1);
/**
 * @author John Flatness, Yu-Hsun Lin
 * @copyright Copyright 2009 John Flatness, Yu-Hsun Lin
 * @copyright BibLibre, 2016
 * @copyright Daniel Berthereau, 2014-2018
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
namespace OaiPmhRepository\OaiPmh\Metadata;

use DOMElement;
use Omeka\Api\Representation\ItemRepresentation;

/**
 * Class implementing metadata output for the dch metadata format.
 * dch is output of the 55 Dublin Core terms.
 *
 * This format is not standardized, but used by some repositories.
 * Note: the namespace and the schema don’t exist. It is designed as an extended
 * version of oai_dc.
 *
 * @link http://www.bl.uk/schemas/
 * @link http://dublincore.org/documents/dc-xml-guidelines/
 * @link http://dublincore.org/schemas/xmls/qdc/dcterms.xsd
 */
class Dch extends AbstractMetadata
{
    /** OAI-PMH metadata prefix */
    const METADATA_PREFIX = 'dch';

    /** XML namespace for output format */
    const METADATA_NAMESPACE = 'http://www.openarchives.org/OAI/2.0/dch/';

    /** XML schema for output format */
    const METADATA_SCHEMA = 'http://www.openarchives.org/OAI/2.0/dch.xsd';

    /** XML namespace for Dublin Core */
    const dch_NAMESPACE_URI = 'http://purl.org/dch/terms/';

    const prefix = "https://dch.iii.u-tokyo.ac.jp";

    /**
     * Appends Dublin Core terms metadata.
     *
     * {@inheritDoc}
     */
    public function appendMetadata(DOMElement $metadataElement, ItemRepresentation $item): void
    {
        $document = $metadataElement->ownerDocument;

        $oai = $document->createElementNS(self::METADATA_NAMESPACE, 'dch:dch');
        $metadataElement->appendChild($oai);

        /* Must manually specify XML schema uri per spec, but DOM won't include
         * a redundant xmlns:xsi attribute, so we just set the attribute
         */
        $oai->setAttribute('xmlns:dc', self::dch_NAMESPACE_URI);
        $oai->setAttribute('xmlns:xsi', parent::XML_SCHEMA_NAMESPACE_URI);
        $oai->setAttribute('xsi:schemaLocation', self::METADATA_NAMESPACE . ' ' .
            self::METADATA_SCHEMA);

        /* Each of the 15 unqualified Dublin Core elements, in the order
         * specified by the dch XML schema
         */
        $localNames = [
            'title',
            'creator',
            'subject',
            'description',
            'publisher',
            'contributor',
            'date',
            'type',
            'format',
            'identifier',
            'source',
            'language',
            'relation',
            'coverage',
            'rights',
        ];

        /* Must create elements using createElement to make DOM allow a
         * top-level xmlns declaration instead of wasteful and non-
         * compliant per-node declarations.
         */
        /*
        foreach ($localNames as $localName) {
            $term = 'dcterms:' . $localName;
            $values = $item->value($term, ['all' => true, 'default' => []]);
            $values = $this->filterValues($item, $term, $values);
            foreach ($values as $value) {
                if($value != ""){
                    $this->appendNewElement($oai, 'dch:' . $localName, (string) $value);
                }
            }
        }
        */

        if ( $item->value( "dcterms:title" ) ) {
            $this->appendNewElement($oai, 'dch:' . "title", (string)$item->value("dcterms:title")->value());
        }

        if ( $item->value( "dcterms:creator" ) ) {
            $this->appendNewElement($oai, 'dch:' . "creator", (string)$item->value("dcterms:creator")->value());
        }

        if ( $item->value( "dcterms:format" ) ) {
            $this->appendNewElement($oai, 'dch:' . "format", (string)$item->value("dcterms:format")->value());
        }

        if ( $item->value( "dcterms:extent" ) ) {
            $this->appendNewElement($oai, 'dch:' . "format", (string)$item->value("dcterms:extent")->value());
        }

        if ( $item->value( "dcterms:publisher" ) ) {
            $this->appendNewElement($oai, 'dch:' . "publisher", (string)$item->value("dcterms:publisher")->value());
        }

        if ( $item->value( "dcterms:date" ) ) {
            $date = (string)$item->value("dcterms:date")->value();
            $this->appendNewElement($oai, 'dch:' . "issued", $date);
            $tmp = explode ("-", $date);
            if (ctype_digit($tmp[0])) {
                $this->appendNewElement($oai, 'dch:' . "year", $tmp[0]);
            }
        }

        if ( $item->value( "dcterms:identifier" ) ) {
            $this->appendNewElement($oai, 'dch:' . "relation", self::prefix."/s/dch/record/".(string)$item->value("dcterms:identifier")->value());
        }

        if ( $item->value( "dcterms:rights" ) ) {
            $this->appendNewElement($oai, 'dch:' . "rights", (string)$item->value("dcterms:rights")->uri());
        }

        $mediaList = $item->media();
        if (!empty($mediaList)) {
            $this->appendNewElement($oai, 'dch:' . "thumbnail", $mediaList[0]->thumbnailUrl("medium"));

            $this->appendNewElement($oai, 'dch:' . "manifestUri", self::prefix."/iiif/".(string)$item->id()."/manifest");
        } else {
            if ( $item->value( "foaf:thumbnail" ) ) {
                $this->appendNewElement($oai, 'dch:' . "thumbnail", (string)$item->value("foaf:thumbnail")->uri());
            }
        }

        $this->appendNewElement($oai, 'dch:' . "seeAlso", self::prefix."/api/items/".(string)$item->id());

        $terms = ["dcterms:title", "dcterms:creator", "dcterms:extent", "dcterms:format",
            "dcterms:identifier", "archiveshub:dateCreatedAccumulatedString",
            "dcterms:date",
            "sc:attributionLabel", "dcterms:publisher",
            //theme
            "dcterms:identifier",
            "sc:viewingDirection",
            "ore:isAggregatedBy",
            "dcterms:relation",
            "dcterms:isPartOf",
            "sc:attributionLabel",
            "ex:l6-050",
            "ex:l6-051",
            "ex:l3-033",
            "ex:l3-034",
            "ex:l3-035",
            "ex:l3-038",
            "ex:l3-039",
            "ex:l3-040",
            "ex:l4-002",
            "ex:l4-003",
            "ex:l4-004",
            "ex:l6-004",
            "ex:l6-005",
            "ex:l6-006",
            "ex:l3-037",
            "ex:l3-038",
            "ex:l3-039",
            "ex:l3-040",
            "ex:l4-005",
            "ex:l4-006",
            "ex:l4-007",
            "ex:l4-008",
            "ex:l6-007",
            "ex:l6-008",
            "ex:l6-009",
            "ex:l6-010",
            "rdfs:seeAlso",
            "dcterms:source",
            "foaf:thumbnail",
            "ex:sort"
            ];

        foreach ($item->values() as $term => $propertyData){
            //var_dump($term);

            if (in_array($term, $terms)){
                continue;
            }

            $p = "";

            if ($propertyData['alternate_label']){
                $p = $propertyData['alternate_label'];
            } else {
                $p = $propertyData['property']->label();
            }

            foreach($propertyData['values'] as $value){
                $v = $value->value();
                if($v != ""){
                    $this->appendNewElement($oai, 'dch:' . "description", $p.": ".$v);
                }

            }

        }

        /*
        $appendIdentifier = $this->singleIdentifier($item);
        if ($appendIdentifier) {
            $this->appendNewElement($oai, 'dch:identifier', $appendIdentifier);
        }

        // Also append an identifier for each file
        if ($this->settings->get('oaipmhrepository_expose_media', false)) {
            foreach ($item->media() as $media) {
                $this->appendNewElement($oai, 'dch:identifier', $media->originalUrl());
            }
        }
        */
    }

    public function getMetadataPrefix()
    {
        return self::METADATA_PREFIX;
    }

    public function getMetadataSchema()
    {
        return self::METADATA_SCHEMA;
    }

    public function getMetadataNamespace()
    {
        return self::METADATA_NAMESPACE;
    }
}
