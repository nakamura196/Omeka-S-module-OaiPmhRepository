<?php declare(strict_types=1);
/**
 * @author John Flatness, Yu-Hsun Lin
 * @copyright Copyright 2009 John Flatness, Yu-Hsun Lin
 * @copyright BibLibre, 2016
 * @copyright Daniel Berthereau, 2014-2018
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
namespace OaiPmhRepository\OaiPmh\Metadata;

use DOMElement;
use Omeka\Api\Representation\ItemRepresentation;

/**
 * Class implementing metadata output for the required dcndl_simple metadata format.
 */
class DCNDLSimple extends AbstractMetadata
{
    /** OAI-PMH metadata prefix */
    const METADATA_PREFIX = 'dcndl_simple';

    /** XML namespace for output format */
    const METADATA_NAMESPACE = 'http://ndl.go.jp/dcndl/dcndl_simple/';

    /** XML schema for output format */
    const METADATA_SCHEMA = 'http://www.openarchives.org/OAI/2.0/dcndl_simple.xsd'; //要検討

    /** XML namespace for unqualified Dublin Core */
    const DC_NAMESPACE_URI = 'http://purl.org/dc/elements/1.1/';

    /** XML namespace for Dublin Core Terms */
    const DCTERMS_NAMESPACE_URI = 'http://purl.org/dc/terms/';

    /** XML namespace for FOAF */
    const FOAF_NAMESPACE_URI = 'http://xmlns.com/foaf/0.1/';

    /**
     * Appends Dublin Core metadata.
     *
     * {@inheritDoc}
     */
    public function appendMetadata(DOMElement $metadataElement, ItemRepresentation $item): void
    {
        $document = $metadataElement->ownerDocument;

        $oai = $document->createElementNS(self::METADATA_NAMESPACE, 'dcndl_simple:dc');
        $metadataElement->appendChild($oai);

        /* Must manually specify XML schema uri per spec, but DOM won't include
         * a redundant xmlns:xsi attribute, so we just set the attribute
         */
        $oai->setAttribute('xmlns:dc', self::DC_NAMESPACE_URI);
        $oai->setAttribute('xmlns:dcterms', self::DCTERMS_NAMESPACE_URI);
        $oai->setAttribute('xmlns:foaf', self::FOAF_NAMESPACE_URI);
        $oai->setAttribute('xmlns:xsi', parent::XML_SCHEMA_NAMESPACE_URI);
        
        //$oai->setAttribute('xsi:schemaLocation', self::METADATA_NAMESPACE . ' ' . self::METADATA_SCHEMA);
        $oai->setAttribute('xsi:schemaLocation', self::METADATA_NAMESPACE);


        $localNames = [
            'title',
            'creator',
            'subject',
            'publisher',
            'contributor',
            'date',
            'type',
            'format',
            'identifier',
            'source',
            'language',
            'relation',
            'coverage',
            'rights',
        ];

        /* Must create elements using createElement to make DOM allow a
         * top-level xmlns declaration instead of wasteful and non-
         * compliant per-node declarations.
         */
        $values = $this->filterValuesPre($item);
        foreach ($localNames as $localName) {
            $term = 'dcterms:' . $localName;
            $termValues = $values[$term]['values'] ?? [];
            $termValues = $this->filterValues($item, $term, $termValues);
            foreach ($termValues as $value) {
                list($text, $attributes) = $this->formatValue($value);
                $this->appendNewElement($oai, 'dc:' . $localName, $text, $attributes);
            }
        }

        $localNames4DCTerms = [
            'description',
        ];

        foreach ($localNames4DCTerms as $localName) {
            $term = 'dcterms:' . $localName;
            $termValues = $values[$term]['values'] ?? [];
            $termValues = $this->filterValues($item, $term, $termValues);
            foreach ($termValues as $value) {
                list($text, $attributes) = $this->formatValue($value);
                $this->appendNewElement($oai, 'dcterms:' . $localName, $text, $attributes);
            }
        }

        $appendIdentifier = $this->singleIdentifier($item);
        if ($appendIdentifier) {
            $this->appendNewElement($oai, 'dc:identifier', $appendIdentifier, ['xsi:type' => 'dcterms:URI']);
        }
        
        /** Thumbnail */
        $medias = $item->media();
        if(count($medias) > 0){
            $media = $medias[0];
            $thumbnail = $media->thumbnailUrl('medium');
            $this->appendNewElement($oai, 'foaf:thumbnail', null, ['rdf:resource' => $thumbnail]);
        }

        /*
        // Also append an identifier for each file
        if ($this->params['expose_media']) {
            foreach ($item->media() as $media) {
                $this->appendNewElement($oai, 'dc:identifier', $media->originalUrl(), ['xsi:type' => 'dcterms:URI']);
            }
        }
        */
    }

    public function getMetadataPrefix()
    {
        return self::METADATA_PREFIX;
    }

    public function getMetadataSchema()
    {
        return self::METADATA_SCHEMA;
    }

    public function getMetadataNamespace()
    {
        return self::METADATA_NAMESPACE;
    }
}
